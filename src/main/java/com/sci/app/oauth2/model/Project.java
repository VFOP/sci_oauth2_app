package com.sci.app.oauth2.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "project")
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "total_budget")
    private Long totalBudget;

    @Column(name = "remaining_budget")
    private Long remainingBudget;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User responsible;

    @JsonIgnore()
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<_Order> orders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTotalBudget() {
        return totalBudget;
    }

    public void setTotalBudget(Long totalBudget) {
        this.totalBudget = totalBudget;
    }

    public Long getRemainingBudget() {
        return remainingBudget;
    }

    public void setRemainingBudget(Long remainingBudget) {
        this.remainingBudget = remainingBudget;
    }

    public User getResponsible() {
        return responsible;
    }

    public void setResponsible(User responsible) {
        this.responsible = responsible;
    }

    public List<_Order> getOrders() {
        return orders;
    }

    public void setOrders(List<_Order> orders) {
        this.orders = orders;
    }
}
