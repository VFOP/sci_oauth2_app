package com.sci.app.oauth2.model.oauth;

import javax.persistence.*;

@Entity
@Table(name = "oauth_code")
public class OAuthCode {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "code")
    private String code;

    @Column(name = "authentication")
    private byte [] authentication;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }
}
