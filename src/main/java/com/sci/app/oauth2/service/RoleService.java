package com.sci.app.oauth2.service;

import com.sci.app.oauth2.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {
    List<Role> getRoles();

}
