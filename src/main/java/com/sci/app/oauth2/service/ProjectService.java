package com.sci.app.oauth2.service;

import com.sci.app.oauth2.model.Project;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjectService {

    List<Project> getProjects();
    Project getProjectById(Long id);
    Project addProject(Project project);
    Project updateProject(Project project);
    void deleteProject(Long id);

}
