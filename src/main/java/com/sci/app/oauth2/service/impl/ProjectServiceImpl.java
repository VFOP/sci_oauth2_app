package com.sci.app.oauth2.service.impl;

import com.sci.app.oauth2.model.Project;
import com.sci.app.oauth2.repository.ProjectRepository;
import com.sci.app.oauth2.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @Override
    public Project getProjectById(Long id) {
        return projectRepository.findOne(id);
    }

    @Override
    public Project addProject(Project project) {
        return projectRepository.save(project);

    }

    @Override
    public Project updateProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public void deleteProject(Long id) {
        projectRepository.delete(id);
    }
}
