package com.sci.app.oauth2.service.impl;

import com.sci.app.oauth2.model.Player;
import com.sci.app.oauth2.repository.PlayerRepository;
import com.sci.app.oauth2.service.PlayerService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    @Override
    public List<Player> getPlayers() {
        return playerRepository.findAll();
    }

    @Override
    public Player getPlayerById(Long playerId) {
        return playerRepository.findOne(playerId);
    }

    @Override
    public List<Player> getPlayersByFirstName(String firstName) {
        return playerRepository.findByFirstName(firstName);
    }

    @Override
    public List<Player> getPlayersByLastName(String lastName) {
        return playerRepository.findByLastName(lastName);
    }

    @Override
    public Player addPlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public void delete(Player player) throws NotFoundException {
        if (player == null)
            throw new NotFoundException("Player entity is null!");

        playerRepository.delete(player);
    }
}
