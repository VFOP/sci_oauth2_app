package com.sci.app.oauth2.service.impl;

import com.sci.app.oauth2.model.CustomUserDetails;
import com.sci.app.oauth2.model.Role;
import com.sci.app.oauth2.model.User;
import com.sci.app.oauth2.repository.RoleRepository;
import com.sci.app.oauth2.repository.UserRepository;
import com.sci.app.oauth2.service.SignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SignupService signupService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Username " + username + " not found!");
        }

        return new CustomUserDetails(user);
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    public User addUser(User user) {

        return signupService.addUser(user);
    }
}
