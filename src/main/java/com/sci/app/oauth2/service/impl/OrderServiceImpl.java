package com.sci.app.oauth2.service.impl;

import com.sci.app.oauth2.model._Order;
import com.sci.app.oauth2.repository.OrderRepository;
import com.sci.app.oauth2.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<_Order> getOrders() {
        return orderRepository.findAll();
    }

    @Override
    public _Order getOrdersById(Long orderId) {
        return orderRepository.getOne(orderId);
    }

    @Override
    public _Order addOrder(_Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void deleteOrder(Long orderId) {
        orderRepository.delete(orderId);
    }

    @Override
    public _Order updateOrder(_Order order) {
        return orderRepository.save(order);
    }
}
