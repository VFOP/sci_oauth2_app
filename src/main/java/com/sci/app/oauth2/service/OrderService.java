package com.sci.app.oauth2.service;

import com.sci.app.oauth2.model._Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {

    List<_Order> getOrders();

    _Order getOrdersById(Long orderId);

    _Order addOrder(_Order order);

    void deleteOrder(Long orderId);

    _Order updateOrder(_Order order);
}
