package com.sci.app.oauth2.service;

import com.sci.app.oauth2.model.Player;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PlayerService {
    List<Player> getPlayers();
    Player getPlayerById(Long id);
    List<Player> getPlayersByFirstName(String firstName);
    List<Player> getPlayersByLastName(String lastName);
    Player addPlayer(Player player);
    void delete(Player player) throws NotFoundException;

}
