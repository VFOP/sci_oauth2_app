package com.sci.app.oauth2.service.impl;

import com.sci.app.oauth2.model.Role;
import com.sci.app.oauth2.repository.RoleRepository;
import com.sci.app.oauth2.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Role> getRoles() {
        return roleRepository.findAll();
    }
}
