package com.sci.app.oauth2.resource;

import com.sci.app.oauth2.model.Role;
import com.sci.app.oauth2.model.User;
import com.sci.app.oauth2.service.SignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class SignupResource {

    @Autowired
    private SignupService signupService;

    @RequestMapping(value = "/signup",
            method = RequestMethod.POST)
    public ResponseEntity<?> signup(@RequestBody User user) {
        user.setRoles(Arrays.asList(new Role("USER")));
        User newUser = signupService.addUser(user);
        if (newUser == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(user, new HttpHeaders(), HttpStatus.CREATED);
    }

}
