package com.sci.app.oauth2.resource;

import com.sci.app.oauth2.model.Project;
import com.sci.app.oauth2.service.ProjectService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectResource extends Resource {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/projects",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Project>> getProjects() {
        List<Project> projects = projectService.getProjects();

        if (projects.isEmpty()) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(projects, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{projectId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> getProjectById(@PathVariable(name = "projectId") Long projectId) {
        Project project = projectService.getProjectById(projectId);

        if (project == null) {
            return new ResponseEntity<Project>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<Project>(project, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/project",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> addProject(@RequestBody Project project) {
        Project newProject = projectService.addProject(project);

        if (newProject == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(newProject, new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/project",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateProject(@RequestBody Project project) {
        Project updatedProject = projectService.updateProject(project);

        if (updatedProject == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(updatedProject, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{projectId}",
            method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> deletePlayer(@PathVariable(name = "projectId") Long projectId) throws NotFoundException {
        projectService.deleteProject(projectId);

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
