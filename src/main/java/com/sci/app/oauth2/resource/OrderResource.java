package com.sci.app.oauth2.resource;

import com.sci.app.oauth2.model._Order;
import com.sci.app.oauth2.service.OrderService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderResource extends Resource {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/orders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<_Order>> getOrders() {

        List<_Order> orders = orderService.getOrders();

        if (orders.isEmpty()) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(orders, new HttpHeaders(), HttpStatus.OK);
    }


    @RequestMapping(value = "/{orderId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<_Order> getProjectById(@PathVariable(name = "orderId") Long orderId) {
        _Order order = orderService.getOrdersById(orderId);

        if (order == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(order, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/order",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<_Order> addOrder(@RequestBody _Order order) {
        _Order newOrder = orderService.addOrder(order);

        if (newOrder == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(newOrder, new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/order",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<_Order> updateOrder(@RequestBody _Order order) {
        _Order updatedOrder = orderService.updateOrder(order);

        if (updatedOrder == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(updatedOrder, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{orderId}",
            method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> deletePlayer(@PathVariable(name = "orderId") Long orderId) throws NotFoundException {
        orderService.deleteOrder(orderId);

        return new ResponseEntity<>(HttpStatus.OK);
    }



}
