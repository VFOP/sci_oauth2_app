package com.sci.app.oauth2.resource;

import com.sci.app.oauth2.model.CustomUserDetails;
import com.sci.app.oauth2.model.User;
import com.sci.app.oauth2.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserResource {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @RequestMapping(value = "/users",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<User>> getUsers() {

        List<User> users = userDetailsService.getUsers();
        if (users.isEmpty()) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(users, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<User> getUserById(@PathVariable(name = "id") Long id) {
        User user = userDetailsService.getUserById(id);

        if (user == null) {
            return new ResponseEntity<User>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);

    }

    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> addUser(@RequestBody User user) {
        User newUser = userDetailsService.addUser(user);

        if (newUser == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(newUser, new HttpHeaders(), HttpStatus.CREATED);

    }

    @RequestMapping(value = "/currentUser",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getCurrentUser() {

        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        User user = userDetailsService.getUserByUsername(customUserDetails.getUsername());
        if (user == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(user, new HttpHeaders(), HttpStatus.OK);
    }

}
