package com.sci.app.oauth2.resource;

import com.sci.app.oauth2.model.Role;
import com.sci.app.oauth2.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/role")
public class RoleResource {

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/roles",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Role>> getRoles() {

        List<Role> roles = roleService.getRoles();

        if (roles.isEmpty()) {
            return new ResponseEntity<List<Role>>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Role>>(roles, new HttpHeaders(), HttpStatus.OK);

    }
}
