package com.sci.app.oauth2.resource;

import com.sci.app.oauth2.model.CustomUserDetails;
import com.sci.app.oauth2.model.Player;
import com.sci.app.oauth2.service.PlayerService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/player/")
public class PlayerResource extends Resource {

    @Autowired
    private PlayerService playerService;

    @RequestMapping(value = "/players",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Player>> getPlayers() {

        List<Player> players = playerService.getPlayers();
        if (players.isEmpty()) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();

        Logger.getLogger(this.getClass().getName()).info("Logged in username: " + customUserDetails.getUsername());

        return new ResponseEntity<>(players, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/fn/{firstName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Player>> getPlayersByFirstName(@PathVariable(value = "firstName") String firstName) {
        List<Player> players = playerService.getPlayersByFirstName(firstName);

        if (players.isEmpty()) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(players, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/ln/{lastName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Player>> getPlayersByLastName(@PathVariable(value = "lastName") String lastName) {
        List<Player> players = playerService.getPlayersByLastName(lastName);

        if (players.isEmpty()) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(players, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/player",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Player> addPlayer(@RequestBody Player player) {
        Player newPlayer = playerService.addPlayer(player);

        if (newPlayer == null) {
            return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(player, new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{playerId}",
            method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> deletePlayer(@PathVariable(name = "playerId") Long playerId) throws NotFoundException {
        Player player = playerService.getPlayerById(playerId);
        playerService.delete(player);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
