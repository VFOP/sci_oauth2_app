package com.sci.app.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SciAppOAuth2Application {

	public static void main(String[] args) {
		SpringApplication.run(SciAppOAuth2Application.class, args);
	}
}
