package com.sci.app.oauth2.repository;

import com.sci.app.oauth2.model.Project;
import com.sci.app.oauth2.model.User;
import com.sci.app.oauth2.model._Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<_Order, Long> {

    List<_Order> findByProject(Project project);
    List<_Order> findByInitiator(User user);
}
