import {Component} from "@angular/core";
import {AuthenticateService} from "../../administration/authenticate/authenticate.service";

@Component({
  selector: 'nav-bar',
  templateUrl: 'navbar.component.html',
  styles: [
    `      
      li > a.active {
        color: greenyellow;
      }
    `
  ]
})
export class NavBarComponent {

  constructor(public authenticateService: AuthenticateService) {
  }

  isLoggedIn () {
    return AuthenticateService
  }

}
