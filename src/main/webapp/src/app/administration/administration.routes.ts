import {Routes} from "@angular/router";
import {AuthGuard} from "../auth-guard.service";
import {AuthenticateComponent} from "./authenticate/authenticate.component";
import {UserListComponent} from "./users/list/user-list.component";
import {SignupComponent} from "./signup/signup.component";
import {ProfileComponent} from "./profile/profile.component";
import {UserListResolverComponent} from "./users/list/user-list.resolver.component";
import {UserResolverComponent} from "./users/view/user-resolver.component";
import {RoleResolverComponent} from "./users/role/role-resolver.component";


export const administrationRoutes: Routes = [

  {
    path: 'users',
    loadChildren: './users/user.module#UserModule',
  },


  {
    path: 'signup',
    component: SignupComponent,
    resolve: {
      role: RoleResolverComponent
    }
  },

  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'login',
    component: AuthenticateComponent
  },

  {
    path: 'logout',
    redirectTo: 'login',
    pathMatch: 'full'
  }
]
