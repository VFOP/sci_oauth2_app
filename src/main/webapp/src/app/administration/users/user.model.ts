
import {RoleModel} from "./role/role.model";

export class UserModel {
  id: number;
  username: string;
  password: string;
  roles : RoleModel[];


  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }



}

