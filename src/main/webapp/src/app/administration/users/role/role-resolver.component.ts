

import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {RoleModel} from "./role.model";
import {Observable} from "rxjs/Observable";
import {RoleService} from "./role.service";

@Injectable()
export class RoleResolverComponent implements Resolve<RoleModel[]>{


  constructor(private roleService: RoleService) {
  }

  resolve(): Observable<RoleModel[]> {
    return this.roleService.getRoles();
  }
}
