

import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {RoleModel} from "./role.model";
import {AuthenticateService} from "../../authenticate/authenticate.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class RoleService {

  private baseUrl: string = 'http://localhost:8080/api/role/';

  constructor(private http: Http) {

  }

  getRoles(): Observable<RoleModel[]> {
    return this.http.get(this.baseUrl + 'roles', AuthenticateService.getHeaders())
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError)
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText)
  }

}
