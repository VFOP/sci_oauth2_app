
import {Component, OnInit} from "@angular/core";
import {UserModel} from "../user.model";
import {ActivatedRoute, Router, Routes} from "@angular/router";
import {RoleModel} from "../role/role.model";
import {UserService} from "../user.service";

@Component({
  selector: 'user-add',
  templateUrl: 'user-add.component.html',
  styles: [`
    .user-add {
      background-color: whitesmoke;
    }

  `
  ]
})
export class UserAddComponent implements OnInit {

  invalidField: boolean;
  user: UserModel;
  roles: RoleModel[];

  me = this;

  constructor(protected route: ActivatedRoute,
              private router: Router,
              protected userService: UserService) {

  }

  ngOnInit(): void {
    if (!this.user) {
      this.user = new UserModel("", "");
    }
    this.roles = this.route.snapshot.data['role']
  }


  saveUser() {
    this.userService.saveUser(this.user).subscribe(
      success => {
        if (success) {
          this.router.navigate(['/users'])
        } else {
          this.invalidField = true;
        }
      },
      failed => {
        this.invalidField = true;
      })
  }

}
