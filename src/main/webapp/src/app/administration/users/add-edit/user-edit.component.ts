
import {Component, OnInit} from "@angular/core";
import {UserModel} from "../user.model";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";
import {RoleModel} from "../role/role.model";

@Component({
  templateUrl: 'user-add.component.html'

})
export class UserEditComponent implements OnInit {
  user: UserModel;
  roles: RoleModel[];

  constructor(protected route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.user = this.route.snapshot.data['user'];
    this.roles = this.route.snapshot.data['role']
  }


}
