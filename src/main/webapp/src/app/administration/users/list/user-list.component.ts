import {Component, OnInit} from "@angular/core";
import {UserModel} from "../user.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'user-list',
  templateUrl: 'user-list.component.html',
  styles: [`
    .user-list {
      background-color: whitesmoke;
    }

  `
  ]
})
export class UserListComponent implements OnInit{

  users: UserModel[];

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.users = this.route.snapshot.data['users']
  }

}
