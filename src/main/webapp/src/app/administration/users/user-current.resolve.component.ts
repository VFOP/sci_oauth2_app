import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {UserModel} from "./user.model";
import {Observable} from "rxjs/Observable";
import {UserService} from "./user.service";

@Injectable()
export class UserCurrentResolveComponent implements  Resolve<UserModel>{
  constructor(private userService: UserService) {
  }

  resolve(): Observable<UserModel>{
    return this.userService.getCurrentUser();
  }

}
