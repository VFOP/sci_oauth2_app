

import {NgModule} from "@angular/core";
import {UserViewComponent} from "./view/user-view.component";
import {UserAddComponent} from "./add-edit/user-add.component";
import {UserDeleteComponent} from "./delete/user-delete.component";
import {UserResolverComponent} from "./view/user-resolver.component";
import {userRoutes} from "./user.routes";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {UserListResolverComponent} from "./list/user-list.resolver.component";
import {UserService} from "./user.service";
import {UserListComponent} from "./list/user-list.component";
import {RoleService} from "./role/role.service";
import {UserEditComponent} from "./add-edit/user-edit.component";
import {RoleResolverComponent} from "./role/role-resolver.component";
import {UserCurrentResolveComponent} from "./user-current.resolve.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(userRoutes)
  ],

  declarations: [
    UserListComponent,
    UserAddComponent,
    UserEditComponent,
    UserViewComponent,
    UserDeleteComponent
  ],

  providers: [
    UserService,
    RoleService,
    UserListResolverComponent,
    UserResolverComponent,
    RoleResolverComponent,
    UserCurrentResolveComponent
  ]
})
export class UserModule {

}
