

import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {UserModel} from "../user.model";
import {Observable} from "rxjs/Observable";
import {UserService} from "../user.service";

@Injectable()
export class UserResolverComponent implements Resolve<UserModel>{


  constructor(private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<UserModel> {
    return this.userService.getUserById(route.params['id']);
  }

}
