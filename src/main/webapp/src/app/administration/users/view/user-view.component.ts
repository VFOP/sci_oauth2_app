import {Component, OnInit} from "@angular/core";
import {UserService} from "../user.service";
import {ActivatedRoute} from "@angular/router";
import {UserModel} from "../user.model";

@Component({
  selector: 'user-view',
  templateUrl: 'user-view.component.html',
  styles: [`
    .user-view {
      background-color: whitesmoke;
    }

  `
  ]
})
export class UserViewComponent implements OnInit {

  user: UserModel;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.forEach((data) => {
      this.user = this.route.snapshot.data['user'];
    })
  }

}
