import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {AuthenticateService} from "../authenticate/authenticate.service";
import {Observable} from "rxjs/Observable";
import {UserModel} from "./user.model";
import {UserModule} from "./user.module";

@Injectable()
export class UserService {

  private baseUrl: string = 'http://localhost:8080/api/user/';


  constructor(private http: Http) {

  }

  getUsers(): Observable<UserModel[]> {
    return this.http.get(this.baseUrl + 'users', AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <UserModel[]>response.json();
      })
      .catch(this.handleError)
  }

  getUserById(id: number): Observable<UserModel> {
    return this.http.get(this.baseUrl + id, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <UserModel>response.json();
      })
      .catch(this.handleError)
  }

  saveUser(user: UserModel): Observable<UserModel> {
    return this.http.post(this.baseUrl + 'user', user, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <UserModel>response.json();
      })
      .catch(this.handleError)
  }

  getCurrentUser(): Observable<UserModel> {
    return this.http.get(this.baseUrl + 'currentUser', AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <UserModel>response.json();
      })
      .catch(this.handleError)
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText)
  }


}
