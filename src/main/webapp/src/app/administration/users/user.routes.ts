

import {Routes} from "@angular/router";
import {UserAddComponent} from "./add-edit/user-add.component";
import {UserViewComponent} from "./view/user-view.component";
import {UserDeleteComponent} from "./delete/user-delete.component";
import {UserListComponent} from "./list/user-list.component";
import {UserListResolverComponent} from "./list/user-list.resolver.component";
import {UserResolverComponent} from "./view/user-resolver.component";
import {RoleResolverComponent} from "./role/role-resolver.component";
import {UserEditComponent} from "./add-edit/user-edit.component";
import {AuthGuard} from "../../auth-guard.service";

export const userRoutes: Routes = [

  {
    path: 'users',
    component: UserListComponent,
    resolve: {
      users: UserListResolverComponent
    },
    canActivate: [AuthGuard]
  },

  {
    path: 'add',
    component: UserAddComponent,
    resolve: {
      role: RoleResolverComponent
    },
    canActivate: [AuthGuard]
  },

  {
    path: 'view/:id',
    component: UserViewComponent,
    resolve: {
      user: UserResolverComponent
    },
    canActivate: [AuthGuard]
  },

  {
    path: 'edit/:id',
    component: UserEditComponent,
    resolve: {
      user: UserResolverComponent,
      role: RoleResolverComponent
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'delete/:id',
    component: UserDeleteComponent,
    resolve: {
      user: UserResolverComponent
    },
    canActivate: [AuthGuard]
  },



]
