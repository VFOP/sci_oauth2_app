import {Component, OnInit} from "@angular/core";
import {JwtHelper} from 'angular2-jwt';
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import {UserModel} from "../users/user.model";
import {AuthenticateService} from "./authenticate.service";

@Component({
  selector: 'login-app',
  templateUrl: './authenticate.component.html'
})
export class AuthenticateComponent implements OnInit {
  invalidLogin: boolean;
  user: UserModel;

  constructor(private http: Http, private router: Router, private authService: AuthenticateService) {
  }

  ngOnInit(): void {
    this.user = new UserModel("", "");
  }

  signIn() {
    this.authService.authenticate(this.user)
      .subscribe(
        success => {
          if (success) {
            this.router.navigate(['/home'])
          } else {
            this.invalidLogin = true;
          }
        },
        failed => {
          this.invalidLogin = true;
        })
  }

}
