import {Injectable} from "@angular/core";
import {Http, RequestOptions, Headers, Response} from "@angular/http";
import {Base64} from 'js-base64';
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {JwtHelper, tokenNotExpired} from "angular2-jwt";
import {Router} from "@angular/router";
import {UserModel} from "../users/user.model";

@Injectable()
export class AuthenticateService {
  constructor(private http: Http, private router: Router) {
  }

  url: string;

  authenticate(user): Observable<boolean> {
    this.url = "http://localhost:8080/oauth/token";

    let params = new URLSearchParams();
    params.append('username', user.username);
    params.append('password', user.password);
    params.append("grant_type", 'password');

    let headers = new Headers({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa("clientId:secret")
    });

    let options = new RequestOptions({headers: headers});

    return this.http.post(this.url, params.toString(), options)
      .map(response => {
        let result = response.json();
        if (result) {
          AuthenticateService.saveToken(result);
          return true;
        } else {
          return false;
        }
      });
  }

  static saveToken(tokenDetails) {
    const expiresAt = JSON.stringify(tokenDetails.expires_in * 1000) + new Date().getTime();
    localStorage.setItem('token', tokenDetails.access_token);
    localStorage.setItem('expires_at', expiresAt)
  }

  static getHeaders() :RequestOptions {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    headers.append('Authorization', 'Bearer ' + token);
    return new RequestOptions({headers: headers});
  }

  isLoggedIn(): boolean{
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at')
    this.router.navigateByUrl('/login');
  }


  private handleError(error: Response) {
    return Observable.throw(error.statusText)
  }

}
