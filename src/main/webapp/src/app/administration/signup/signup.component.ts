import {Component} from "@angular/core";
import {UserAddComponent} from "../users/add-edit/user-add.component";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../users/user.service";

@Component({
  templateUrl: '../users/add-edit/user-add.component.html'
})
export class SignupComponent extends UserAddComponent{


  constructor(route: ActivatedRoute, router: Router, userService: UserService) {
    super(route, router, userService);
  }
}
