import {SignupComponent} from "./signup/signup.component";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {AuthenticateComponent} from "./authenticate/authenticate.component";
import {administrationRoutes} from "./administration.routes";
import {ProfileComponent} from "./profile/profile.component";
import {UserListComponent} from "./users/list/user-list.component";
import {UserService} from "./users/user.service";
import {UserListResolverComponent} from "./users/list/user-list.resolver.component";
import {UserModule} from "./users/user.module";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserModule,
    RouterModule.forChild(administrationRoutes)
  ],

  declarations: [
    AuthenticateComponent,
    SignupComponent,
    ProfileComponent,
  ],

  providers: [

  ]
})

export class AdministrationModule {

}
