import {Component, OnInit} from "@angular/core";
import {ProjectAddComponent} from "../add/project-add.component";
import {ProjectModel} from "../project.model";

@Component({
  selector: 'project-edit',
  templateUrl: 'project-edit.component.html',
  styles: [
      `
      .project-edit {
        background-color: whitesmoke;
      }
    `
  ]
})
export class ProjectEditComponent extends ProjectAddComponent implements OnInit {

  project: ProjectModel;


  ngOnInit(): void {
    this.project = this.route.snapshot.data['project'];
  }

  updateProject(): void {
    this.projectService.updateProject(this.project)
      .subscribe(
        success => {
          if (success) {
            this.router.navigate(['/projects'])
          } else {
            this.invalidField = true;
          }
        },
        failed => {
          this.invalidField = false;
        }
      )
  }

}
