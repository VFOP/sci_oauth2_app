

import {Routes} from "@angular/router";
import {ProjectListComponent} from "./list/project-list.component";
import {ProjectListResolverComponent} from "./list/project-list.resolver.component";
import {ProjectAddComponent} from "./add/project-add.component";
import {ProjectEditComponent} from "./edit/project-edit.component";
import {ProjectDeleteComponent} from "./delete/project-delete.component";
import {AuthGuard} from "../auth-guard.service";
import {ProjectViewComponent} from "./view/project-view.component";
import {ProjectResolverComponent} from "./view/project-resolver.component";
import {UserCurrentResolveComponent} from "../administration/users/user-current.resolve.component";

export const projectRoutes: Routes = [
  {
    path: 'projects',
    component: ProjectListComponent,
    resolve: {
      project: ProjectListResolverComponent
    },
    canActivate: [AuthGuard]

  },
  {
    path: 'add',
    component: ProjectAddComponent,
    canActivate: [AuthGuard],
    resolve: {
      responsible: UserCurrentResolveComponent
    }
  },
  {
    path: 'view/:id',
    component: ProjectViewComponent,
    resolve: {
      project: ProjectResolverComponent
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: ProjectEditComponent,
    canActivate: [AuthGuard],
    resolve: {
      project: ProjectResolverComponent,
    }
  },
  {
    path: 'delete/:id',
    component: ProjectDeleteComponent,
    canActivate: [AuthGuard]
  }
]
