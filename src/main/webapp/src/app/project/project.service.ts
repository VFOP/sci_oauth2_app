import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {ProjectModel} from "./project.model";
import {AuthenticateService} from "../administration/authenticate/authenticate.service";

@Injectable()
export class ProjectService {
  private baseUrl: string = 'http://localhost:8080/api/project/';

  constructor(private http: Http) {
  }

  getProjects(): Observable<ProjectModel[]> {
    return this.http.get(this.baseUrl + 'projects', AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <ProjectModel[]>response.json()
      })
      .catch(this.handleError)
  }


  getProjectById(id: number): Observable<ProjectModel> {
    return this.http.get(this.baseUrl + id, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <ProjectModel>response.json();
      })
      .catch(this.handleError)
  }

  saveProject(project: ProjectModel): Observable<ProjectModel> {
    return this.http.post(this.baseUrl + 'project', project, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <ProjectModel>response.json();
      })
      .catch(this.handleError)
  }

  updateProject(project: ProjectModel): Observable<ProjectModel> {
    return this.http.put(this.baseUrl + 'project', project, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <ProjectModel> response.json();
      })
      .catch(this.handleError)
  }

  deleteProject(id: number) {
    return this.http.delete(this.baseUrl + id, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError);
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText)
  }
}
