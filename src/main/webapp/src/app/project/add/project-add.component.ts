
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../project.service";
import {ProjectModel} from "../project.model";
import {UserModel} from "../../administration/users/user.model";

@Component({
  selector: 'project-add',
  templateUrl: 'project-add.component.html',
  styles: [
    `
      .project-add {
        background-color: whitesmoke;
      }
    `
  ]
})
export class ProjectAddComponent implements OnInit{

  invalidField: boolean;
  project: ProjectModel;
  responsible: UserModel;

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected projectService: ProjectService) {

  }

  ngOnInit(): void {
    if (!this.project) {
      this.responsible = this.route.snapshot.data['responsible'];
      this.project = new ProjectModel('', '', 0, 0, this.responsible);
    }
  }


  saveProject() {
    this.projectService.saveProject(this.project).subscribe(
      success => {
        if (success) {
          this.router.navigate(['/projects'])
        } else {
          this.invalidField = true;
        }
      },
      failed => {
        this.invalidField = true;
      })
  }


}
