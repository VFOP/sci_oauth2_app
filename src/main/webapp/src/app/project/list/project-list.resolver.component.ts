

import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {ProjectListComponent} from "./project-list.component";
import {ProjectModel} from "../project.model";
import {Observable} from "rxjs/Observable";
import {ProjectService} from "../project.service";

@Injectable()
export class ProjectListResolverComponent implements Resolve<ProjectModel[]>{

  constructor(private projectService: ProjectService) {
  }

  resolve(): Observable<ProjectModel[]> {
    return this.projectService.getProjects();
  }

}
