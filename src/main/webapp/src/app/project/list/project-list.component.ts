import {Component, OnInit} from "@angular/core";
import {ProjectModel} from "../project.model";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../project.service";

@Component({
  selector: 'project-list',
  templateUrl: 'project-list.component.html',
  styles: [
      `
      .project-list {
        background-color: white;
      }
    `
  ]
})
export class ProjectListComponent implements OnInit {

  projects: ProjectModel[];

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected projectService: ProjectService) {
  }

  ngOnInit(): void {
    this.projects = this.route.snapshot.data['project'];
  }

  deleteProject(id: number): void {
    debugger;
    this.projectService.deleteProject(id)
      .subscribe(
        success => {
          this.router.navigate(['/projects'])
        },
        failed => {

        }
      )
  }

}
