
import {Component, OnInit} from "@angular/core";
import {ProjectModel} from "../project.model";
import {ActivatedRoute, Route} from "@angular/router";

@Component({
  selector: 'project-view',
  templateUrl: 'project-view.component.html',
  styles: [`
    .project-view {
      background-color: whitesmoke;
    }

  `
  ]
})
export class ProjectViewComponent implements OnInit{

  project: ProjectModel;


  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.forEach((data) => {
      this.project = this.route.snapshot.data['project'];
    })
  }

}
