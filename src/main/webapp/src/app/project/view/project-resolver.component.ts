
import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {ProjectModel} from "../project.model";
import {Observable} from "rxjs/Observable";
import {ProjectService} from "../project.service";

@Injectable()
export class ProjectResolverComponent implements Resolve<ProjectModel>{

  constructor(private projectService: ProjectService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<ProjectModel> {
    return this.projectService.getProjectById(route.params['id']);
  }

}
