
import {NgModule} from "@angular/core";
import {projectRoutes} from "./project.routes";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {ProjectListComponent} from "./list/project-list.component";
import {ProjectService} from "./project.service";
import {ProjectListResolverComponent} from "./list/project-list.resolver.component";
import {ProjectViewComponent} from "./view/project-view.component";
import {ProjectAddComponent} from "./add/project-add.component";
import {ProjectEditComponent} from "./edit/project-edit.component";
import {ProjectDeleteComponent} from "./delete/project-delete.component";
import {ProjectResolverComponent} from "./view/project-resolver.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(projectRoutes)
  ],

  declarations: [
    ProjectListComponent,
    ProjectViewComponent,
    ProjectAddComponent,
    ProjectEditComponent,
    ProjectDeleteComponent
  ],

  providers: [
    ProjectService,
    ProjectResolverComponent,
    ProjectListResolverComponent,
  ]
})
export class ProjectModule {

}
