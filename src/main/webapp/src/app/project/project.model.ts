

import {UserModel} from "../administration/users/user.model";
import {OrderModel} from "../order/order.model";

export class ProjectModel {
  id: number;
  name: string;
  description: string;
  totalBudget: number;
  remainingBudget: number;
  responsible: UserModel;
  orders: OrderModel[];


  constructor(name: string,
              description: string,
              totalBudget: number,
              remainingBudget: number,
              responsible: UserModel) {
    this.name = name;
    this.description = description;
    this.totalBudget = totalBudget;
    this.remainingBudget = remainingBudget;
    this.responsible = responsible;
  }
}
