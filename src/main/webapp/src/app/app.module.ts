import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {routes} from "./routes";
import {AuthGuard} from "./auth-guard.service";
import {FormsModule} from "@angular/forms";
import {Http, HttpModule, RequestOptions} from "@angular/http";
import {HomeComponent} from "./home/home.component";
import {AuthenticateService} from "./administration/authenticate/authenticate.service";
import {CommonModule} from "@angular/common";
import {NavBarComponent} from "./utils/nav/navbar.component";
import {AdministrationModule} from "./administration/administration.module";
import {BsDropdownModule} from "ngx-bootstrap";
import {PlayerService} from "./player/player.service";
import {PlayerListResolverComponent} from "./player/player-list.resolver.component";
import {PlayerThumbnailComponent} from "./player/player-thumbnail.component";
import {PlayerListComponent} from "./player/player-list.component";
import {AuthConfig, AuthHttp} from "angular2-jwt";
import {UserService} from "./administration/users/user.service";
import {ProjectListComponent} from "./project/list/project-list.component";
import {ProjectListResolverComponent} from "./project/list/project-list.resolver.component";
import {ProjectService} from "./project/project.service";
import {OrderService} from "./order/order.service";
import {OrderListComponent} from "./order/list/order-list.component";
import {OrderListResolverComponent} from "./order/list/order-list.resolver.component";
import {ProjectModule} from "./project/project.module";
import {OrderModule} from "./order/order.module";


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BsDropdownModule.forRoot(),
    FormsModule,
    HttpModule,
    ProjectModule,
    OrderModule,
    AdministrationModule,
    RouterModule.forRoot(routes, {useHash: true})
  ],
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    PlayerThumbnailComponent,
    PlayerListComponent,
  ],
  providers: [
    AuthGuard,
    AuthenticateService,
    PlayerService,
    PlayerListResolverComponent,

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
