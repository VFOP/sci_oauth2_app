import {RouterModule, Routes} from "@angular/router";
import {AuthGuard} from "./auth-guard.service";
import {HomeComponent} from "./home/home.component";
import {PlayerListComponent} from "./player/player-list.component";
import {PlayerListResolverComponent} from "./player/player-list.resolver.component";
import {ProjectListComponent} from "./project/list/project-list.component";
import {ProjectListResolverComponent} from "./project/list/project-list.resolver.component";
import {OrderListResolverComponent} from "./order/list/order-list.resolver.component";
import {OrderListComponent} from "./order/list/order-list.component";

export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'player',
    component: PlayerListComponent,
    canActivate: [AuthGuard],
    resolve: {
      player: PlayerListResolverComponent
    }
  },

  {
    path: 'projects',
    loadChildren: './project/project.module#ProjectModule',
  },

  {
    path: 'orders',
    loadChildren: './order/order.module#OrderModule',
  },

  {
    path: 'administration',
    loadChildren: './administration/administration.module#AdministrationModule',
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }

]
