import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {OrderModel} from "./order.model";
import {AuthenticateService} from "../administration/authenticate/authenticate.service";

@Injectable()
export class OrderService {

  private baseUrl: string = 'http://localhost:8080/api/order/';

  constructor(private http: Http) {
  }

  getOrders(): Observable<OrderModel[]> {
    return this.http.get(this.baseUrl + 'orders', AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <OrderModel[]>response.json()
      })
      .catch(this.handleError)
  }

  getOrderById(id: number): Observable<OrderModel> {
    return this.http.get(this.baseUrl + id, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <OrderModel>response.json()
      })
      .catch(this.handleError)
  }

  saveOrder(order: OrderModel): Observable<OrderModel> {
    return this.http.post(this.baseUrl + 'order', order, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <OrderModel>response.json();
      })
      .catch(this.handleError)
  }

  updateOrder(order: OrderModel): Observable<OrderModel> {
    return this.http.put(this.baseUrl + 'order', order, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <OrderModel> response.json();
      })
      .catch(this.handleError)
  }

  deleteOrder(id: number) {
    return this.http.delete(this.baseUrl + id, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return response.json();
      })
      .catch(this.handleError)
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText)
  }
}
