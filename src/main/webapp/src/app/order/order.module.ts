
import {NgModule} from "@angular/core";
import {OrderListComponent} from "./list/order-list.component";
import {OrderViewComponent} from "./view/order-view.component";
import {OrderAddComponent} from "./add/order-add.component";
import {OrderEditComponent} from "./edit/order-edit.component";
import {OrderDeleteComponent} from "./delete/order-delete.component";
import {OrderService} from "./order.service";
import {OrderListResolverComponent} from "./list/order-list.resolver.component";
import {OrderResolverComponent} from "./view/order-resolver.component";
import {orderRoutes} from "./order.routes";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(orderRoutes)


  ],

  declarations: [
    OrderListComponent,
    OrderViewComponent,
    OrderAddComponent,
    OrderEditComponent,
    OrderDeleteComponent
  ],

  providers: [
    OrderService,
    OrderListResolverComponent,
    OrderResolverComponent
  ]
})
export class OrderModule {

}
