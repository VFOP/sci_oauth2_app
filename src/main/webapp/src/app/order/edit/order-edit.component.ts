import {Component, OnInit} from "@angular/core";
import {OrderModel} from "../order.model";
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from "@angular/router";
import {de} from "ngx-bootstrap";
import {ProjectModel} from "../../project/project.model";
import {OrderAddComponent} from "../add/order-add.component";
import {OrderService} from "../order.service";
import {UserService} from "../../administration/users/user.service";

@Component({
  selector: 'order-edit',
  templateUrl: 'order-edit.component.html',
  styles: [
    `.order-edit {
      background-color: whitesmoke;
    }
    `
  ]
})
export class OrderEditComponent extends OrderAddComponent implements OnInit {

  order: OrderModel;
  projects: ProjectModel[];


  ngOnInit(): void {
    this.order = this.route.snapshot.data['order'];
    this.projects = this.route.snapshot.data['projects'];
  }

  updateOrder(): void {
    this.orderService.updateOrder(this.order).subscribe(
      success => {
        if (success) {
          this.router.navigate(['/orders'])
        } else {
          this.invalidField = true;
        }
      },
      failed => {
        this.invalidField = true;
      })



  }
}

