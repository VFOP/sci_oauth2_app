import {Component, OnInit} from "@angular/core";
import {OrderModel} from "../order.model";
import {ProjectModel} from "../../project/project.model";
import {ActivatedRoute, Router} from "@angular/router";
import {UserModel} from "../../administration/users/user.model";
import {OrderService} from "../order.service";
import {UserService} from "../../administration/users/user.service";
import {AuthenticateService} from "../../administration/authenticate/authenticate.service";

@Component({
  selector: 'order-add',
  templateUrl: 'order-add.component.html',
  styles: [`
    .order-add {
      background-color: whitesmoke;
    }
  `
  ]
})
export class OrderAddComponent implements OnInit {

  invalidField: boolean;
  order: OrderModel;
  projects: ProjectModel[];
  initiator: UserModel;
  project: ProjectModel;


  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected orderService: OrderService) {

  }

  ngOnInit(): void {
    if (!this.order) {
      this.initiator = this.route.snapshot.data['currentUser'];
      this.projects = this.route.snapshot.data['projects'];

      this.order = new OrderModel(this.initiator,
        "", 0, false);
    }
  }

  saveOrder() {
    this.orderService.saveOrder(this.order).subscribe(
      success => {
        if (success) {
          this.router.navigate(['/orders'])
        } else {
          this.invalidField = true;
        }
      },
      failed => {
        this.invalidField = true;
      })
  }


}
