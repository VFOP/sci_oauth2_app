
import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {OrderModel} from "../order.model";
import {OrderService} from "../order.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class OrderResolverComponent implements Resolve<OrderModel>{

  constructor(private orderService: OrderService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<OrderModel> {
    return this.orderService.getOrderById(route.params['id']);
  }

}
