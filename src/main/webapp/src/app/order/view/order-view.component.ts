
import {Component, OnInit} from "@angular/core";
import {OrderModel} from "../order.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'order-view',
  templateUrl: 'order-view.component.html',
  styles: [`    
    .order-view {
      background-color: whitesmoke;
    }
  `]
})
export class OrderViewComponent implements OnInit{

  order: OrderModel;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.order = this.route.snapshot.data['order'];
  }

}
