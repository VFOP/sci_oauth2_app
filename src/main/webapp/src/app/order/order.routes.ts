import {Routes} from "@angular/router";
import {OrderListComponent} from "./list/order-list.component";
import {AuthGuard} from "../auth-guard.service";
import {OrderListResolverComponent} from "./list/order-list.resolver.component";
import {OrderAddComponent} from "./add/order-add.component";
import {OrderViewComponent} from "./view/order-view.component";
import {OrderResolverComponent} from "./view/order-resolver.component";
import {OrderEditComponent} from "./edit/order-edit.component";
import {OrderDeleteComponent} from "./delete/order-delete.component";
import {UserCurrentResolveComponent} from "../administration/users/user-current.resolve.component";
import {ProjectListResolverComponent} from "../project/list/project-list.resolver.component";

export const orderRoutes: Routes = [

  {
    path: 'orders',
    component: OrderListComponent,
    canActivate: [AuthGuard],
    resolve: {
      orders: OrderListResolverComponent
    }
  },
  {
    path: 'add',
    component: OrderAddComponent,
    canActivate: [AuthGuard],
    resolve: {
      projects: ProjectListResolverComponent,
      currentUser: UserCurrentResolveComponent
    }
  },

  {
    path: 'view/:id',
    component: OrderViewComponent,
    canActivate: [AuthGuard],
    resolve: {
      order: OrderResolverComponent
    }
  },

  {
    path: 'edit/:id',
    component: OrderEditComponent,
    canActivate: [AuthGuard],
    resolve: {
      order: OrderResolverComponent,
      projects: ProjectListResolverComponent
    }
  },
  {
    path: 'delete/:id',
    component: OrderDeleteComponent,
    canActivate: [AuthGuard]
  }
]
