import {Injectable} from "@angular/core";
import {OrderModel} from "../order.model";
import {ActivatedRoute, Resolve} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {OrderService} from "../order.service";

@Injectable()
export class OrderListResolverComponent implements Resolve<OrderModel[]> {

  constructor(private orderService: OrderService) {
  }

  resolve(): Observable<OrderModel[]> {
    return this.orderService.getOrders();
  }


}
