import {Component, OnInit} from "@angular/core";
import {OrderModel} from "../order.model";
import {ActivatedRoute, Router} from "@angular/router";
import {OrderService} from "../order.service";

@Component({
  selector: 'order-list',
  templateUrl: 'order-list.component.html',
  styles: [
      `
      .order-list {
        background-color: white;
      }
    `
  ]
})
export class OrderListComponent implements OnInit {
  orders: OrderModel[];


  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected orderService: OrderService) {
  }

  ngOnInit(): void {
    this.orders = this.route.snapshot.data['orders'];
  }

  deleteOrder(id: number): void {
    debugger;
    this.orderService.deleteOrder(id)
      .subscribe(
        success => {
          this.router.navigate(['/orders'])
        },
        failed => {
        }
      )
  }

}
