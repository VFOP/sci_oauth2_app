

import {UserModel} from "../administration/users/user.model";
import {ProjectModel} from "../project/project.model";

export class OrderModel {
  id:number;
  initiator: UserModel;
  destination: string;
  budget: number;
  approved: boolean;
  project: ProjectModel;


  constructor(initiator: UserModel,
              destination: string,
              budget: number,
              approved: boolean) {
    this.initiator = initiator;
    this.destination = destination;
    this.budget = budget;
    this.approved = approved;
  }
}
