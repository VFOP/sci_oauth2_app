import {Input, Component} from "@angular/core";
import {Http} from "@angular/http";
import {Router} from "@angular/router";
import {AuthenticateService} from "../administration/authenticate/authenticate.service";

@Component({
  selector: 'home-app',
  templateUrl: 'home.component.html'
})
export class HomeComponent {
  @Input('loggedInUser') loggedInUser: string;

  constructor(private http: Http, private router: Router, private authService: AuthenticateService) {
  }

}
