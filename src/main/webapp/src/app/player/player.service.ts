import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {IPlayer} from "./player.model";
import {AuthenticateService} from "../administration/authenticate/authenticate.service";

@Injectable()
export class PlayerService {
  private url: string = 'http://localhost:8080/api/player/players';

  constructor(private http: Http) {
  }

  getPlayers(): Observable<IPlayer[]> {
    return this.http.get(this.url, AuthenticateService.getHeaders())
      .map((response: Response) => {
        return <IPlayer[]>response.json()
      })
      .catch(this.handleError)
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText)
  }
}
