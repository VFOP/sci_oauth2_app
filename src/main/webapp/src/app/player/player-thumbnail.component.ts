import {Component, Input} from "@angular/core";
import {IPlayer} from "./player.model";

@Component({
  selector: 'player-thumbnail',
  templateUrl: 'player-thumbnail.component.html'
})

export class PlayerThumbnailComponent {
  @Input() player: IPlayer
}
