import {Component, OnInit} from "@angular/core";
import {IPlayer} from "./player.model";
import {PlayerService} from "./player.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  templateUrl: 'player-list.component.html'
})
export class PlayerListComponent implements OnInit{
  players: IPlayer[];


  constructor(private route:ActivatedRoute) {
  }

  ngOnInit() {
    this.players = this.route.snapshot.data['player']
  }
}
