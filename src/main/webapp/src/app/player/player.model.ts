

export interface IPlayer {
  id: number,
  firstName: string,
  lastName: string,
  age: number

}
