import {Resolve} from "@angular/router";
import {IPlayer} from "./player.model";
import {PlayerService} from "./player.service";
import {Observable} from "rxjs/Observable";
import {Injectable} from "@angular/core";

@Injectable()
export class PlayerListResolverComponent implements Resolve<IPlayer[]> {
  constructor(private playerService: PlayerService) {

  }

  resolve(): Observable<IPlayer[]> {
    return this.playerService.getPlayers();
  }
}
