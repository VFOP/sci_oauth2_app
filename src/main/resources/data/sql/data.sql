INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity)
VALUES
  ('clientId', 'secret', 'read,write,trust', 'password,refresh_token', 'ROLE_CLIENT,ROLE_TRUSTED_CLIENT', 900, 2592000);
INSERT INTO player (id, age, first_name, last_name) VALUES (1, 31, 'RAFAEL', 'NADAL');
INSERT INTO player (id, age, first_name, last_name) VALUES (2, 36, 'ROGER', 'FEDERER');
INSERT INTO player (id, age, first_name, last_name) VALUES (3, 30, 'NOVAK', 'DJOKOVIC');
INSERT INTO player (id, age, first_name, last_name) VALUES (4, 30, 'ANDY', 'MURRAY');
INSERT INTO player (id, age, first_name, last_name) VALUES (5, 26, 'GRIGOR', 'DIMITROV');

INSERT INTO role (id, name) VALUES (1, 'ADMIN');
INSERT INTO role (id, name) VALUES (2, 'USER');
INSERT INTO _USER (ID, PASSWORD, USERNAME)
VALUES (1, '$2a$10$BOVWkYlsbaWmtIoLDYYLGOfBIPoDi2yXN1Ui158SIXPNsrRGNYwh.', 'admin');
INSERT INTO _USER (ID, PASSWORD, USERNAME)
VALUES (2, '$2a$10$BOVWkYlsbaWmtIoLDYYLGOfBIPoDi2yXN1Ui158SIXPNsrRGNYwh.', 'user');
INSERT INTO user_roles (USER_ID, ROLE_ID) VALUES (1, 1);
INSERT INTO user_roles (USER_ID, ROLE_ID) VALUES (1, 2);
INSERT INTO user_roles (USER_ID, ROLE_ID) VALUES (2, 2);